const canvas = document.getElementById('canvas'),
	ctx = canvas.getContext('2d'),
	addValue = document.getElementById('addValue'),
	addButton = document.getElementById('addButton'),
	removeValue = document.getElementById('removeValue'),
	removeButton = document.getElementById('removeButton');

ctx.scale(canvas.width / 100, canvas.height / 50);
ctx.lineWidth = 0.5;
ctx.textAlign = "center";
ctx.textBaseline = "middle";
ctx.font = "5px sans-serif";

class TreeNode {
	constructor(value, parent) {
		this.value = value;
		this.depth = 0;
		this.parent = parent;
		this.before = null;
		this.after = null;
	}
	
	get beforeDepth() {
		return (this.before !== null ? this.before.depth + 1 : 0);
	}
	
	get afterDepth() {
		return (this.after !== null ? this.after.depth + 1 : 0);
	}
}

class PriorityQueue {
	constructor(comparer) {
		this.comparer = comparer;
		this.root = null;
		this.first = null;
	}
	
	enqueue(value) {
		if (this.root === null) {
			this.root = new TreeNode(value, null);
			this.first = value;
		} else {
			if (this.comparer(value, this.first) > 0) this.first = value;
			let child = this.root, node, before, depth = 0, difference;
			while (child !== null) {
				node = child;
				before = this.comparer(value, node.value) > 0;
				child = before ? node.before : node.after;
			}
			if (before) node.before = new TreeNode(value, node);
			else node.after = new TreeNode(value, node);
			
			while (node !== null && node.depth < ++depth) {
				node.depth = depth;
				difference = node.beforeDepth - node.afterDepth;
				if (difference > 1 || difference < -1) {
					let rotate = (root, isLeft) => {
						let pivot = isLeft ? root.before : root.after;
						if (isLeft) {
							root.before = pivot.after;
							if (root.before !== null) root.before.parent = root;
							pivot.after = root;
						} else {
							root.after = pivot.before;
							if (root.after !== null) root.after.parent = root;
							pivot.before = root;
						}
						pivot.parent = root.parent;
						if (root.parent === null) this.root = pivot;
						else if (root.parent.before === root) root.parent.before = pivot;
						else root.parent.after = pivot;
						root.parent = pivot;
						
						root.depth = Math.max(root.beforeDepth, root.afterDepth);
						pivot.depth = Math.max(pivot.beforeDepth, pivot.afterDepth);
					}
					
					if (difference > 0 && child.beforeDepth < child.afterDepth ||
						difference < 0 && child.beforeDepth > child.afterDepth)
						rotate(child, difference < 0);
					
					rotate(node, difference > 0);
					return;
				}
				child = node;
				node = node.parent;
			}
		}
	}
	
	dequeue(value) {
		let node = this.root, last = null, found, compared, before, direction, moved = false;
		while (node !== null && (compared = this.comparer(value, node.value)) !== 0) {
			before = compared > 0;
			node = before ? node.before : node.after;
		}
		if (node === null) return false;
		found = node;
		
		direction = found.beforeDepth > found.afterDepth;
		node = direction ? node.before : node.after;
		while (node !== null) {
			last = node;
			node = direction ? node.after : node.before;
			if (node !== null) moved = true;
		}
		
		if (last !== null) {
			if (this.comparer(found.value, this.first) === 0) this.first = last.value;
			node = direction ? last.before : last.after;
			found.value = last.value;
			if (direction !== moved) last.parent.before = node;
			else last.parent.after = node;
			if (node !== null) node.parent = last.parent;
			node = last;
		} else if (found.parent !== null) {
			node = found;
			if (this.comparer(found.value, this.first) === 0) this.first = found.parent.value;
			if (before) found.parent.before = null;
			else found.parent.after = null;
		} else {
			this.root = null;
			this.first = null;
		}
		
		if (node) {
			let oldDepth;
			do {
				node = node.parent;
				oldDepth = node.depth;
				node.depth = Math.max(node.beforeDepth, node.afterDepth);
			} while (node.parent !== null && node.depth !== oldDepth);
		}
		
		return true;
	}
	
	find(value) {
		let node = this.root, compared;
		while (node !== null && (compared = this.comparer(value, node.value)) !== 0)
			node = compared > 0 ? node.before : node.after;
		return node !== null ? node.value : null;
	}
}

let queue = new PriorityQueue((a, b) => a < b ? 1 : a > b ? -1 : 0);

addButton.onclick = function() {
	queue.enqueue(parseInt(addValue.value));
	drawTree();
};

removeButton.onclick = function() {
	queue.dequeue(parseInt(removeValue.value));
	drawTree();
};

function drawTree() {
	function drawNode(node) {
		function drawNext(before) {
			ctx.beginPath();
			ctx.moveTo(50, 15);
			ctx.save();
			ctx.scale(0.5, 0.5);
			ctx.translate(before ? 0 : 100, 50);
			ctx.lineTo(50, 5);
			ctx.stroke();
			drawNode(before ? node.before : node.after);
			ctx.restore();
		}
		
		ctx.beginPath();
		ctx.arc(50, 10, 5, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.fillText(node.value, 50, 10);
		
		if (node.before) drawNext(true);
		if (node.after) drawNext(false);
	}
	
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	if (queue.root) drawNode(queue.root);
}
